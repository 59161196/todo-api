const express = require('express')//import library
const app = express()//สร้าง application //()เรียกใช้fuction

let todos = [
    {
        name: 'nong',
        id: 1
    },
    {
        name: 'supatsorn',
        id: 2
    }
]
//SELECT * FROM TODO //ดึงข้อมูลจากtodo,1.มีapp 2.function //res=respository
app.get('/todos', (req, res) => {
    res.send(todos)
})

//INSERT INTO TODO
app.post('/todos', (req, res) => {
    let newTOdo = {
        name: 'Read a book',
        id: 3
    }
    todos.push(newTOdo) //todoแรกคือtodoที่อยู่ข้างบน ,push เพื่ม
    res.status(201).send()
})

app.listen(3000, () => {
    console.log('TODO API Started at a port 3000')
})
